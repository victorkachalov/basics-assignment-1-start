import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomAlertComponent } from './random-alert.component';

describe('RandomAlertComponent', () => {
  let component: RandomAlertComponent;
  let fixture: ComponentFixture<RandomAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandomAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
