import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-random-alert]',						/* attribute - style selector */
  templateUrl: './random-alert.component.html',
  styleUrls: ['./random-alert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RandomAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
