import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-warning-alert',							/* standard - style selector */
  templateUrl: './warning-alert.component.html',
  styleUrls: ['./warning-alert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WarningAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
